variable name {
  description = "Environment name"
  type        = string
}

variable project_id {
  description = "Project ID in GCP"
  type        = string
}

variable zone {
  description = "Region"
  type        = string
}

variable worker_disk_size {
  description = "Boot disk size (GB) for worker nodes"
  type        = number
  default     = 20
}

variable worker_machine_type {
  description = "Worker node's machine type"
  type        = string
  default     = "n1-standard-2"
}

variable k8s_version {
  description = "Kubernetes version"
  type        = string
  default     = "1.15.11"
}