name                = "infra"
project_id          = "aerobic-cyclist-276712"
zone                = "europe-north1-b"
worker_disk_size    = 20
worker_machine_type = "e2-medium"
k8s_version         = "1.15.11"