provider "google" {
  project = var.project_id
  region  = "europe-north1"
  zone    = var.zone
}

data google_container_engine_versions "engine_versions" {
  location       = var.zone
  version_prefix = var.k8s_version
}

data "google_client_config" "provider" {}

data "google_container_cluster" "cluster" {
  name     = var.name
  location = var.zone
}

resource google_container_cluster "cluster" {
  project                  = var.project_id
  provider                 = google-beta
  name                     = var.name
  location                 = var.zone
  remove_default_node_pool = true
  initial_node_count       = 1
  monitoring_service       = "none"
  logging_service          = "none"
  min_master_version       = data.google_container_engine_versions.engine_versions.latest_master_version

  network_policy {
    enabled = false
  }
  addons_config {
    istio_config {
      disabled = true
    }
  }
  maintenance_policy {
    daily_maintenance_window {
      start_time = "02:00"
    }
  }
}

resource google_container_node_pool "node_pool" {
  name               = var.name
  location           = var.zone
  cluster            = google_container_cluster.cluster.name
  version            = data.google_container_engine_versions.engine_versions.latest_node_version
  initial_node_count = 1

  autoscaling {
    min_node_count = 1
    max_node_count = 10
  }

  node_config {
    preemptible  = true
    machine_type = var.worker_machine_type
    disk_size_gb = var.worker_disk_size

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/monitoring.write",
    ]
  }

  timeouts {
    create = "60m"
    update = "60m"
    delete = "60m"
  }
}



provider "kubernetes" {
  load_config_file = false

  host  = "https://${data.google_container_cluster.cluster.endpoint}"
  token = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(
  data.google_container_cluster.cluster.master_auth[0].cluster_ca_certificate,
  )
}